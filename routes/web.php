<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',         'StatisticsController@buildInputForm');
Route::post('/stats',   'StatisticsController@generateStats');

Route::get('/stats', function () {
    return redirect('/');
});
