@extends('analyser.base')

@section('content')
    <div class="title m-b-md">
        Phrase Analyser
    </div>

    {!! form($form) !!}
@endsection