@extends('analyser.base')

@section('content')
    <div class="title m-b-md">
        Phrase Analyser
    </div>

    <div class="phrase-input">
        Phrase :
        <code>
            "{{ $phraseInput }}"
        </code>
    </div>

    <div class="stat-grid-results">
        @foreach($stats as $char => $stat)
            <p>
                {{ $char }} :
                {{ $stat['occurrence'] }} :

                before:
                @if(!empty($stat['before']))
                    {{ implode(",", $stat['before']) }}
                @else
                    none
                @endif

                after:
                @if(!empty($stat['after']))
                    {{ implode(",", $stat['after']) }}
                @else
                    none
                @endif

                @if(isset($stat['max-distance']))
                    max-distance: {{ $stat['max-distance'] }} chars
                @endif
            </p>
        @endforeach
    </div>

@endsection