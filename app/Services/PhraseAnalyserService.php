<?php

namespace App\Services;

use Fhaculty\Graph\Edge\Directed;
use Fhaculty\Graph\Graph;
use Fhaculty\Graph\Vertex;

class PhraseAnalyserService implements AnalyserInterface
{
    /**
     * walk graph and generate statistics
     * (node's occurrence, sibling, max-distance between occurrence)
     *
     * @param $phrase
     *
     * @return array
     */
    public function generateStats($phrase)
    {
        $graph = $this->generateGraph($phrase);
        $stats = [];

        /** @var Vertex $vertex */
        foreach($graph->getVertices() as $vertex) {
            $vertexId = $vertex->getId();

            $stats[$vertexId]['occurrence'] = $vertex->getAttribute('occurrence');

            $maxDistance = $vertex->getAttribute('max-distance');
            if(isset($maxDistance)) {
                $stats[$vertexId]['max-distance'] = $maxDistance;
            }

            /** @var Directed $edge */
            foreach($vertex->getEdgesIn()->getEdgesDistinct() as $edge) {
                $stats[$vertexId]['after'][] = $edge->getVertexStart()->getId();
            }

            /** @var Directed $edge */
            foreach($vertex->getEdgesOut()->getEdgesDistinct() as $edge) {
                $stats[$vertexId]['before'][] = $edge->getVertexEnd()->getId();
            }
        }

        return $stats;
    }

    /**
     * Analyse data and generate graph with useful data (occurrence and max distance)
     * @param $phrase
     *
     * @return Graph
     */
    private function generateGraph($phrase)
    {
        $phrase     = strtolower($phrase);
        $charArray  = preg_split( '//u', $phrase, null, PREG_SPLIT_NO_EMPTY );

        $graph = new Graph();

        /** @var Vertex $currentVertex */
        $currentVertex = null;

        foreach ($charArray as $index => $char) {
            //only register non space characters
            if(trim($char) == '') {
                $currentVertex = null;
                continue;
            }

            /**
             * @var Vertex $beforeVertex
             * Set precedent vertex
             */
            $beforeVertex   = $currentVertex;

            // if char/vertex already encountered
            if ($graph->getVertices()->hasVertexId($char)) {
                $currentVertex = $graph->getVertices()->getVertexId($char);

                /** @var int $occurrence */
                $occurrence = $currentVertex->getAttribute('occurrence');
                $currentVertex->setAttribute('occurrence', $occurrence + 1);

                /** @var int $lastPosition */
                $lastPosition   = $currentVertex->getAttribute('position');
                $maxDistance    = $currentVertex->getAttribute('max-distance', 0);
                $currentVertex->setAttribute('max-distance', max($maxDistance, $index - $lastPosition - 1));
            } else {
                $currentVertex  = $graph->createVertex($char);
                $currentVertex->setAttribute('occurrence', 1);
            }

            // in both cases
            $currentVertex->setAttribute('position', $index);

            //create the link to the constructed graph
            if(isset($beforeVertex)) {
                $beforeVertex->createEdgeTo($currentVertex);
            }
        }

        return $graph;
    }
}