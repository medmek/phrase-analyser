<?php


namespace App\Services;

use Fhaculty\Graph\Graph;

interface AnalyserInterface
{
    /**
     * analyse data and generate statistics
     * @param $data
     *
     * @return Graph
     */
    public function generateStats($data);
}