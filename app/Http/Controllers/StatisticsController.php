<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Services\AnalyserInterface;

class StatisticsController extends BaseController
{
    /**
     * @var AnalyserInterface
     */
    protected $analyserService;

    /**
     * StatisticsController constructor.
     *
     * @param AnalyserInterface $analyserService
     */
    public function __construct(AnalyserInterface $analyserService)
    {

        $this->analyserService = $analyserService;
    }

    /**
     * @param FormBuilder $formBuilder
     *
     * @return View
     */
    public function buildInputForm(FormBuilder $formBuilder)
    {
        //generate simple form
        $form = $formBuilder->plain([
            'method'    => 'POST',
            'url'       => action('StatisticsController@generateStats')
        ])
            ->add('phrase', 'text', [
                'label' => 'Input Text to Analyse',
                'rules' => 'required|max:255',
                ])
            ->add('submit', 'submit', ['label' => 'Analyse']);

        return view('analyser.input', compact('form'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateStats(Request $request)
    {
        $phraseInput = $request->get('phrase');

        $stats = $this->analyserService->generateStats($phraseInput);

        return view('analyser.stats', compact( 'stats', 'phraseInput'));
    }
}